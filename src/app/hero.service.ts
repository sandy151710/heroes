import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Hero } from './hero';
import { HEROES } from './mock-heroes';

@Injectable()
export class HeroService {

  constructor() { }
  //The HeroService must wait for the server to respond, getHeroes() cannot
  // return immediately with hero data, and the browser will not block while the service waits
  //get heroes will not return the data synchronously 
  // getHeroes(): Hero[] {
  //   return HEROES;
  // }

  getHeroes(): Observable<Hero[]> {
    return of(HEROES);
  }

}
